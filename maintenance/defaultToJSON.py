import json

with open ('WikiDesigner/resources/styles/default.css', 'r') as default:
    data = {}
    currentTheme = ''
    for line in default:
        line = line.strip(' \n;\{\}')
        if not line:
            pass
        elif line.startswith('--'):
            var = line.split(':')
            if(len(var)) == 2:
                data[currentTheme][var[0]] = var[1].strip()
            else:
                print(f'skipped: \"{var}\"')
        elif line.startswith('}'):
            pass
        else:
            currentTheme = line
            data[currentTheme] = {}
    
    with open('WikiDesigner/resources/styles/default.json.txt', 'w') as output:
        output.write(json.dumps(data).replace('"', '\\"'))