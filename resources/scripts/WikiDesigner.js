/**
 * @class mw.WikiDesigner
 * @singleton
 */
mw.WikiDesigner = {

    // stylesheet : '',
    // parsedStylesheet: {},

    // _constructor(){
    //     //this.stylesheet = this.getStylesheet();
    //     //this.parseStylesheet = this.parseStylesheet();
    // },

    sanitizeHex(hex){
        // cast to string
        hex = String(hex);
        // remove # and spaces
        hex = hex.replace('#', '');
        hex = hex.replace(' ', '');

        // convert the hex into 6 digit
        if(hex.length == 4){
            hex = hex.slice(0,3);
        }
        else if(hex.length == 8){
            hex = hex.slice(0, 6);
        }

        if(hex.length == 3){
            hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
        }
        else if(hex.length != 6) {
            // something is wrong, fallback to avoid errors
            return '000000';
        }

        return hex;
    },

    hexToRgb(hex){
        // get a hex string
        // return an array of length 3 containing r, g, b integers
        hex = this.sanitizeHex(hex);

        return [Number('0x' + hex.slice(0,2)), Number('0x' + hex.slice(2,4)), Number('0x' + hex.slice(4,6))];
    },

    rgbToHex(rgb){
        // get an array of length 3 containing r, g, b integers
        // return a hex string
        console.log(rgb);
        hex = '';

        hex += Math.round(rgb[0]).toString(16).padStart(2, '0');
        hex += Math.round(rgb[1]).toString(16).padStart(2, '0');
        hex += Math.round(rgb[2]).toString(16).padStart(2, '0');

        return hex;
    },

    hexToHsv(hex){

        // Convert from hex to integer, then convert to percentage of the maximum amount (255)
        let rgb = this.hexToRgb(hex);
        let red = rgb[0]/255;
        let green = rgb[1]/255;
        let blue = rgb[2]/255;

        let h, s, v;

        // Value is simply the maximum value of our colors
        v = Math.max(red, green, blue);

        // The formula for Hue depends on which of our colors has the greatest value. Since we already calculated that as V, we can reuse the variable
        if (v = Math.min(red, green, blue)) {
            //if all colors are equal, we're just going to stick H at 0 to avoid a NaN
            h = 0;
        }
        else {
            switch (v){

            case red:
                h = (green - blue) / (r - Math.min(g, b));
                break;

            case green:
                h = 2 + (blue - red) / (green - Math.min(red, blue));
                break;

            case blue:
                h = 4 + (red - green) / (blue - Math.min(red, green));
                break;
            }

        }

        // Convert Hue to a positive degree measurement on the color wheel
        h = h * 60
        if (h < 0) {
            h += 360;
        }

        // We're reusing V as our maximum color value again for calculating saturation
        // Only do the formula if V is not equal to 0 to avoid a divide-by-zero error
        if (v != 0) {
            s = (v - Math.min(red, green, blue)) / v;
        }
        else {
            s = 0;
        }

        // We no longer need these as percentages, and they suit our final purposes better as integers
        s = s * 100;
        v = v * 100;

        return [h, s, v];

    },

    HSVToHex(hsv){

    },

    getStylesheet(){
        return document.getElementById('wikiDesigner__preview');
    },

    // parseStylesheet(){
    //     for(let line of this.stylesheet.textContent.split('\n')){
    //         line = line.trim();
    //         if(line.startsWith('--')){
    //             line = line.split(':');
    //             this.parsedStylesheet[line[0].trim()] = line[1].trim().replace(';', '');
    //         }
    //     }
    // },

    getValue(key){
        let re = new RegExp(`${key}: *(.+?);`);
        return this.getStylesheet().textContent.match(re)[1];
    },

    updateSidebarOpacity(value){
        this.updateStylesheet('--wiki-sidebar-background-opacity', value);
    },

    updateContentOpacity(value){
        this.updateStylesheet('--wiki-content-background-opacity', value);
    },

    updateStylesheet(key, value){
        let re = new RegExp(`${key}: *.+?;`);
        this.getStylesheet().textContent = this.getStylesheet().textContent.replace(re, `${key}: ${value};`);

        if(value.trim().startsWith('#')){
            re = new RegExp(`${key}--rgb: *.+?;`);
            this.getStylesheet().textContent = this.getStylesheet().textContent.replace(re, `${key}--rgb: ${this.hexToRgb(value).join(', ')};`);
        }

        console.log(key, value);

        switch (key) {
            case '--wiki-body-background-color':
                this.updateDynamicColors(key, value);
                this.updateStylesheet(
                    '--wiki-body-background-color--secondary',
                    this.calculateMix(
                        value,
                        this.getValue('--wiki-body-dynamic-color--inverted'),
                        0.75
                    )
                )
                break;
            case '--wiki-content-background-color':
                this.updateDynamicColors(key, value);
                this.updateMixColors();
                this.updateStylesheet('--wiki-content-text-color', this.calculateTextColor(value));
                this.updateStylesheet(
                    '--wiki-content-background-color--secondary',
                    this.calculateMix(
                        value,
                        this.getValue('--wiki-content-dynamic-color'),
                        0.85
                    )
                );
                this.updateStylesheet(
                    '--wiki-navigation-background-color--secondary',
                    this.calculateMix(
                        this.getValue('--wiki-navigation-background-color'),
                        value,
                        0.25
                    )
                );
                this.updateGeneralIconFilter(value);
                break;
            case '--wiki-content-text-color':
                this.updateMixColors();
                break;
            case '--wiki-content-link-color':
            case '--wiki-accent-color':
                this.updateLabelColors(key, value);
                break;
            case '--wiki-navigation-background-color':
                this.updateStylesheet(
                    '--wiki-navigation-background-color--secondary',
                    this.calculateMix(
                        value,
                        this.getValue('--wiki-content-background-color'),
                        0.25
                    )
                );
                break;
        }
    },

    calculateLuminance(hex){
        // get the luminance value (https://www.w3.org/TR/WCAG20/#relativeluminancedef) of a hex and return it as a float

        rgb = this.hexToRgb(hex);
        return (0.2126 * rgb[0] / 255) + (0.7152 * rgb[1] / 255) + (0.0722 * rgb[2] / 255);
    },

    calculateMix(hex1, hex2, ratio){
        if(ratio == 0){
            return hex1;
        }

        // convert the hex values to rgb
        rgb1 = this.hexToRgb(hex1);
        rgb2 = this.hexToRgb(hex2);

        // use ratio to get a weighted average of each color,
        // then convert it back to hex
        return '#' + this.rgbToHex([
            (rgb1[0] * ratio) + (rgb2[0] * (1 - ratio)),
            (rgb1[1] * ratio) + (rgb2[1] * (1 - ratio)),
            (rgb1[2] * ratio) + (rgb2[2] * (1 - ratio)),
        ]);
    },

    calculateTextColor(hex){
        hex = this.sanitizeHex(hex);

        if (this.calculateLuminance(hex) > 0.5) {
            return('#111111');
        }
        else {
            return('#eeeeee');
        }
    },

    updateMixColors(){
        let hex1 = this.getValue('--wiki-content-background-color');
        let hex2 = this.getValue('--wiki-content-text-color');
        this.updateStylesheet('--wiki-content-text-mix-color', this.calculateMix(hex1, hex2, 0.5));
        this.updateStylesheet('--wiki-content-text-mix-color--95', this.calculateMix(hex1, hex2, 0.95));
    },

    updateDynamicColors(key, hex){
        key = key.replace('-background-color', '-dynamic-color');

        if (this.calculateLuminance(hex) > 0.5) {
            this.updateStylesheet(key, '#000000');
            this.updateStylesheet(key + '--inverted', '#ffffff');
            this.updateStylesheet(key + '--secondary', '#333333');
            this.updateStylesheet(key + '--secondary--inverted', '#dddddd');
        }
        else {
            this.updateStylesheet(key, '#ffffff');
            this.updateStylesheet(key + '--inverted', '#000000');
            this.updateStylesheet(key + '--secondary', '#dddddd');
            this.updateStylesheet(key + '--secondary--inverted', '#333333');
        }
    },

    updateLabelColors(key, hex){
        key = key.replace('-color', '-label-color');

        if (this.calculateLuminance(hex) > 0.5) {
            this.updateStylesheet(key, '#000000');
        }
        else {
            this.updateStylesheet(key, '#ffffff');
        }
    },

    updateGeneralIconFilter(hex){
        if(this.calculateLuminance(hex) > 0.5) {
            this.updateStylesheet('--wiki-icon-general-filter', '');
        }
        else {
            this.updateStylesheet('--wiki-icon-general-filter', 'invert(100%)');
        }
    },

    save(){
        const api = new mw.Api();
        api.post({
            "action": "edit",
            "format": "json",
            "title": "Mediawiki:WikiDesigner.css",
            "text": this.getStylesheet().textContent,
            "summary": "",
            //"tags": "wikidesigner",
            "formatversion": "2",
            "token": mw.user.tokens.get('csrfToken'),
        }).then(function(data){
            if(data.edit.result == "Success"){
                console.log("Success");
            }
            else {
                console.log("Something went wrong. Response follows:");
                console.log(data);
            }
        });
    }

};