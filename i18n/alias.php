<?php
/**
 * Aliases for wikidesigner
 *
 * @file
 * @ingroup Extensions
 */

$specialPageAliases = [];

/** English
 * @author Mr Pie 5
 */
$specialPageAliases['en'] = [
	'WikiDesigner' => [ 'Wiki_Designer', 'WikiDesigner'],
];