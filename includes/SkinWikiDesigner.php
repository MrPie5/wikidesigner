<?php
namespace MediaWiki\Extension\WikiDesigner;

use OutputPage;
use SkinMustache;

class SkinWikiDesigner extends SkinMustache {
	/**
	 * @param OutputPage $out
	 */
	public function initPage( OutputPage $out ) {
		parent::initPage( $out );
		$out->disableClientCache();
	}

	/**
	 * @inheritDoc
	 * @return array
	 */
	public function getTemplateData() {
		$out = $this->getOutput();
		return [
			'html-body-content' => $this->wrapHTML( $out->getTitle(), $out->getHTML() ),
		] + parent::getTemplateData();
	}
}
