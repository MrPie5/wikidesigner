<?php
/**
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @file
 */

namespace MediaWiki\Extension\WikiDesigner;

use MediaWiki\ResourceLoader as RL;

class Hooks implements
	\MediaWiki\Hook\BeforePageDisplayHook,
	\MediaWiki\ResourceLoader\Hook\ResourceLoaderRegisterModulesHook {

	/**
	 * @see https://www.mediawiki.org/wiki/Manual:Hooks/BeforePageDisplay
	 * @param \OutputPage $out
	 * @param \Skin $skin
	 */
	public function onBeforePageDisplay( $out, $skin ): void {
		// zext IS NOT A TYPO
		// the module list is sorted alphabetically, and I need this module to be loaded after the built-in MediaWiki/Vector stylesheets, so I added a z to the name.
		// I am furious that this works. Please someone tell me there's a better way.
		$out->addModuleStyles( [ /* not a typo! */'zext.wikiDesigner.global.styles' ] );

	}

	public function onResourceLoaderSiteStylesModulePages( $skin, &$pages ) {
		$pages['MediaWiki:WikiDesigner.css'] = [ 'type' => 'style' ];
	}

	public function onListDefinedTags( array &$tags ) {
		$tags[] = 'wikidesigner';
	}

	public function onChangeTagsListActive( array &$tags ) {
		$tags[] = 'wikidesigner';
	}

	/**
	 * @see https://www.mediawiki.org/wiki/Manual:Hooks/ResourceLoaderRegisterModules
	 * @param RL\ResourceLoader $resourceLoader
	 */
	public function onResourceLoaderRegisterModules( RL\ResourceLoader $resourceLoader ): void {
		// Hardcoded extension install path, there must be a way to get this right
		$baseDir = 'extensions/WikiDesigner/';

		// Inject our overrides into modules with reflection, as there is no built-in way in MediaWiki to achieve that.
		// Reflection is... less than ideal, but it does the job.
		foreach ( ( include __DIR__ . '/StyleResourceOverrides.php' ) as $moduleName => $extraFiles ) {
			$module = $resourceLoader->getModule( $moduleName );

			// Only FileModules are supported below.
			if ( !( $module instanceof RL\FileModule ) ) {
				continue;
			}

			// In typical modules, append to the CSS files list. However, in OOUI file modules append to skin-specific sets, as
			// they predictably output skin styles after module supplied files in their bundles.
			// See RL\OOUIFileModule::extendSkinSpecific for official documentation on that behavior.
			$property = ( new \ReflectionObject( $module ) )->getProperty( $module instanceof RL\OOUIFileModule ? 'skinStyles'
				: 'styles' );
			$property->setAccessible( true ); // Required prior to PHP 8.1
			$value = $property->getValue( $module );

			// Append all our extra files to the module's styles array and fix up paths
			if ( $module instanceof RL\OOUIFileModule ) {
				foreach ( $value as $_ => &$skinSpecific ) {
					foreach ( $extraFiles as &$filePath ) {
						$skinSpecific[] = $baseDir . $filePath;
					}
				}
			} else {
				foreach ( $extraFiles as &$filePath ) {
					$value[] = $baseDir . $filePath;
				}
			}

			$property->setValue( $module, $value );
		}
	}

}
